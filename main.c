/*
 * Copyright (c) 2015, 2016 Glenn Wurr III <glenn@wurr.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>
#include <stdio.h>
#include "rs485.h"

// use an extended ASCII code ( 128 and up ) as an address so it doesn't get confused with normal data
#define MYADDY 187

unsigned char data_count = 0;
unsigned char is_command = 0;
unsigned char data_in[255];

char command[8];
char argv[244];


char output[255];



void copy_command (void) {
	// packet example: ID-":"-<command 8 bytes>-":"-<arguments>"
	// 
	// so in the sender program, it might look like this:
	//unsigned char wbuf[] = " :PING    :TROLOLOLOLOLOLO This is an argument \n";
	//wbuf[0] = 128; // the address byte goes in that empty whitespace in the above

	// Copy the command stuff out of data_in into command and argv
	memcpy(command, data_in+2, 8); 
	memcpy(argv, data_in+11, 244); 

	// Now clear data_in, process_rs485 can reuse it
	memset(data_in, 0, 255); 
}


void process_command(void)
{
	unsigned int do_output = 0;

	// some example commands

	if (strstr(command, "PING") != NULL) {
		// Please note that as we're sending the address as a numerical,
		// instead of a char, we get 2 less bytes. So if argv is full,
		// the last two bytes are gonna get chopped off on the echo back
		snprintf(output , 255 , "%d:PONG    :%s\n" , MYADDY , argv);
		do_output = 1;
	} else if (strstr(command, "SETPWM1") != NULL) {
		OCR0A = atoi(argv);
		snprintf(output , 255 , "%d:ACK     :SETPWM1 to %s\n" , MYADDY , argv);
		do_output = 1;

	} else if (strstr(command, "SETPWM2") != NULL) {
		OCR0B = atoi(argv);
		snprintf(output , 255 , "%d:ACK     :SETPWM2 to %s\n" , MYADDY , argv);
		do_output = 1;

	}






	if (do_output) {
		_delay_ms(100); // makes timing easier on PC RS485 adapter
		rs485_send(output);
  		memset(output, 0, 255); // clear output buffer for next run JIC
	}




}


void process_uart(void){

	unsigned int c = rs485_readc();
	
	if ( c & 256 ){ // rs485_readc returns 256 if no data in ring buffer
	  	// no data available from UART 
	} else if ( c == MYADDY || is_command == 1){ // if byte is our address or if we've already recv'd our adddy
	
		is_command = 1;	
		// Add char to input buffer
		data_in[data_count] = c;
		
		// newline is signal for end of command input
		if (data_in[data_count] == '\n') {
			// Reset to 0, ready to go again
			data_count = 0;
			is_command = 0;	
			
			copy_command();
			process_command();
			// clear ring buffers for good measure
			// some noise may have occured in the time it took to run process_command.
			rs485_clear(); 
		} else {
			data_count++;
		}
		
	} else {
		//do nothing
	}
}





int main(void)
{


        // PWM on  ATmega48A/PA/88A/PA/168A/PA/328/P example
        // Port D5 and D6 as output
        DDRD   |= (1 << 6); 
        DDRD   |= (1 << 5); 
	TCCR0A = _BV(COM0A1) | _BV(COM0B1) | _BV(WGM01) | _BV(WGM00);      // Non inverting mode on OC0A and OC0B, Mode = Mode 3 FAST PWM
	TCCR0B = _BV(CS00);                                                // No prescaling
	// default to off
	OCR0A = 0;
	OCR0B = 0;




	rs485_init();
	sei();
	_delay_ms(500);
	while(1) {
		process_uart();
	}



}

