/* 
 * This stuff is mostly stolen from https://github.com/kohyama/AVR-RS485
 * I just added some tweaks and the rs485_readc() function.
 * Giving credit:
 * Copyright (c) 2011, Yoshinori Kohyama (http://algobit.jp/)
 * Thanks, man :)
 * https://raw.githubusercontent.com/kohyama/AVR-RS485/85617bd9d8eebc119cbd212697fd09836aefe442/LICENSE.md
 * It's a one-clause BSD license ( only source code, nothing about binary format )
 * So is compatible with any BSD/MIT/etc licesne
 */
/*
 * Copyright (c) 2011, Yoshinori Kohyama (http://algobit.jp/)
 * Copyright (c) 2016, Glenn Wurr III
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 *
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>
#include <stdlib.h>





// TODO: use something like this for BAUD
/** @brief  UART Baudrate Expression
 *  @param  xtalcpu  system clock in Mhz, e.g. 4000000UL for 4Mhz          
 *  @param  baudrate baudrate in bps, e.g. 1200, 2400, 9600     
 */
//#define UART_BAUD_SELECT(baudRate,xtalCpu)  (((xtalCpu) + 8UL * (baudRate)) / (16UL * (baudRate)) -1UL)



// TODO: Make this portable to other AVRs with some ifdef shit


static volatile struct {
	char *buf;
	int size;
	int read;
	int write;
} rs485_rx, rs485_tx;

void rs485_init(void)
{
	rs485_rx.size = 256;
	rs485_rx.buf = (char *)malloc(rs485_rx.size * sizeof(char));
	rs485_rx.read = 0;
	rs485_rx.write = 0;
	rs485_tx.size = 256;
	rs485_tx.buf = (char *)malloc(rs485_tx.size * sizeof(char));
	rs485_tx.read = 0;
	rs485_tx.write = 0;

	DDRD |= 0x04; // PD2:Dir
	PORTD &= ~0x04; // start in RX mode
	_delay_us(500);

	// checkout http://wormfood.net/avrbaudcalc.php
	//UBRR0 = 12; // 38400bps under CPU 8MHz
	//UBRR0 = 25; // 38400bps under CPU 16MHz
	UBRR0 = 103; // 9600bps under CPU 16MHz
	//UBRR0 = 3332; // 300bps under CPU 16MHz
	//UBRR0 = 832; // 1200bps under CPU 16MHz
	UCSR0A = 0;
	UCSR0B = _BV(RXCIE0)|_BV(RXEN0)|_BV(TXEN0);
	UCSR0C = _BV(UCSZ01)|_BV(UCSZ00);
}

void rs485_clear(void)
{
	cli();
	memset(rs485_rx.buf, 0, rs485_rx.size);
	memset(rs485_tx.buf, 0, rs485_tx.size);
	rs485_rx.read = 0;
	rs485_rx.write = 0;
	rs485_tx.read = 0;
	rs485_tx.write = 0;
	sei();
}

void rs485_send(char *p)
{
	int next;

	if (*p == '\0')
		return;

	cli();
	next = (rs485_tx.write + 1)%rs485_tx.size;
	while (next != rs485_tx.read && *p != '\0') { //while there is still data or haven't reached EOL
		rs485_tx.buf[rs485_tx.write] = *p++; //put char into buffer
		rs485_tx.write = next;
		next = (rs485_tx.write + 1)%rs485_tx.size;
	}
	PORTD |= 0x04; // set TX mode on MAX485 to on
	_delay_us(500); // wait for MAX485 to turn on
	UCSR0B |= _BV(UDRIE0); // enable TX interrupt
	sei(); //re-enable interrupts so USART_UDRE_vect will fire

	while (rs485_tx.write != rs485_tx.read  ) //block, until buffer has been sent
		_delay_us(500); 
}

int rs485_readln(char *buf, int size)
{
	int n = 0;

	do {
		cli();
		while (rs485_rx.read != rs485_rx.write && n + 1 < size) { //while read buffer still has data or we hit max size
			buf[n++] = rs485_rx.buf[rs485_rx.read]; //shove char into arg buffer
			rs485_rx.read = (rs485_rx.read + 1)%rs485_rx.size; //counter
		}
		sei();
	} while (n == 0 || buf[n - 1] != 0x0a ); // repeat until we hit a newline char 

	buf[n] = '\0'; // set last char to null terminator

	return n;
}

unsigned int rs485_readc(void)
{
	unsigned char data;


	if (rs485_rx.read == rs485_rx.write ) { //if nothing to read from buffer
		return 256;
	}

	data =  rs485_rx.buf[rs485_rx.read];
	rs485_rx.read = (rs485_rx.read + 1)%rs485_rx.size;

	return data;

}



ISR(USART_RX_vect)
{
	int next;

	cli();
	next = (rs485_rx.write + 1)%rs485_rx.size; // update counter
	rs485_rx.buf[rs485_rx.write] = UDR0; //read a char into buffer
	if (next != rs485_rx.read)
		rs485_rx.write = next;
	sei();
}

ISR(USART_UDRE_vect)
{
	cli();
	if (rs485_tx.read == rs485_tx.write) { // if buffer empty
		_delay_us(5000); // without this the last char often gets cut off short
		// could be cutting of max485 right as it's in transit. 16mhz is pretty fast...
		UCSR0B &= ~_BV(UDRIE0); // disable transmit interrupt
		PORTD &= ~0x04; // turn off TX mode on MAX485
		_delay_us(500); // give MAX485 some time to change modes

	} else { // buffer not empty, we're sending
		UDR0 = rs485_tx.buf[rs485_tx.read]; // send a char
		rs485_tx.read = (rs485_tx.read + 1)%rs485_tx.size; // update counter
	}
	sei();
}


