/* 
 * This stuff is mostly stolen from https://github.com/kohyama/AVR-RS485
 * I just added some tweaks and the rs485_readc() function.
 * Giving credit:
 * Copyright (c) 2011, Yoshinori Kohyama (http://algobit.jp/)
 * Thanks, man :)
 * https://raw.githubusercontent.com/kohyama/AVR-RS485/85617bd9d8eebc119cbd212697fd09836aefe442/LICENSE.md
 * It's a one-clause BSD license ( only source code, nothing about binary format )
 * So is compatible with any BSD/MIT/etc licesne
 */

#ifndef _RS485_H
#define _RS485_H

void rs485_init(void);
void rs485_clear(void);
void rs485_send(char *p);
int rs485_readln(char *buf, int size);
unsigned int rs485_readc(void);

#endif /* _RS485_H */
